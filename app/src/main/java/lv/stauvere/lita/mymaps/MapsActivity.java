package lv.stauvere.lita.mymaps;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static java.lang.Thread.*;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, Toolbar.OnMenuItemClickListener {

    public static final String TAG = "inlineDebug";

    private static final int FINE_LOCATION_PERMISSION_REQUEST = 500;
    private GoogleMap mMap;
    Location mLastLocation;
    private static final float DEFAULT_ZOOM = 15f;

    ArrayList<LatLng> markerPoints;
    Marker marker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        //Toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setTitle("Karte");
        toolbar.inflateMenu(R.menu.menu);
        toolbar.setOnMenuItemClickListener(this);

        if (googleServicesAvailable()) {
            initMap();
        } else {
            Toast.makeText(this, "Google Play ielādes kļūda!", Toast.LENGTH_LONG).show();
        }

    }

    public boolean googleServicesAvailable() {
        GoogleApiAvailability api = GoogleApiAvailability.getInstance();
        int isAvailable = api.isGooglePlayServicesAvailable(this);
        if (isAvailable == ConnectionResult.SUCCESS) {
            return true;
        } else if (api.isUserResolvableError(isAvailable)) {
            Dialog dialog = api.getErrorDialog(this, isAvailable, 0);
            dialog.show();
        } else {
            Toast.makeText(this, "Nevar pieslēgties Google Play pakalpojumam", Toast.LENGTH_SHORT).show();
        }
        return false;
    }

    private void initMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.setTrafficEnabled(false);
        mMap.getUiSettings().setMapToolbarEnabled(false);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, FINE_LOCATION_PERMISSION_REQUEST);
            try {
                sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            recreate();

        } else {

            getDeviceLocation();
            mMap.setMyLocationEnabled(true);

            //Navigācijas punktu uzstādīšana
            markerPoints = new ArrayList<>();

            mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

                @Override
                public void onMapClick(LatLng latLng) {

                    if(markerPoints.size() > 1){
                        markerPoints.clear();
                        mMap.clear();
                        punkti();
                    }

                    markerPoints.add(latLng);

                    MarkerOptions options = new MarkerOptions();
                    options.position(latLng);

                    if(markerPoints.size()==1){
                        options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
                    }else if(markerPoints.size()==2) {
                        options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                    }

                    mMap.addMarker(options);

                    if(markerPoints.size() >= 2){
                        LatLng origin = markerPoints.get(0);
                        LatLng dest = markerPoints.get(1);
                        Log.d(TAG, "origin: " + origin + " dest: " + dest);
                        String url = getDirectionsUrl(origin, dest);
                        Log.d(TAG, "url: " + url);
                        DownloadTask downloadTask = new DownloadTask();
                        downloadTask.execute(url);
                    }
                }
            });

            punkti();
        }

        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                @Override
                public View getInfoWindow(Marker marker) {
                    return null;
                }

                @Override
                public View getInfoContents(Marker marker) {
                    View v = getLayoutInflater().inflate(R.layout.info_window, null);

                    TextView tvLocality = v.findViewById(R.id.tv_locality);
                    TextView tvLat = v.findViewById(R.id.tv_lat);
                    TextView tvLng = v.findViewById(R.id.tv_lng);
                    TextView tvSnippet = v.findViewById(R.id.tv_snippet);

                    LatLng vieta = marker.getPosition();
                    tvLocality.setText(marker.getTitle());
                    tvLat.setText("Latitude: " + vieta.latitude);
                    tvLng.setText("Longitude: " + vieta.longitude);
                    tvSnippet.setText(marker.getSnippet());

                    return v;
                }
            });

    }

    private void getDeviceLocation() {
        Log.d(TAG, "getDeviceLocation: iegūst pašreizējo atrašanās vietu");
        FusedLocationProviderClient mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        try {
            Task location = mFusedLocationProviderClient.getLastLocation();
            location.addOnCompleteListener(this, new OnCompleteListener() {
                @Override
                public void onComplete(@NonNull Task task) {
                    if (task.isSuccessful() && task.getResult() != null) {
                        mLastLocation = (Location) task.getResult();
                        Log.d(TAG, "onComplete: vieta atrasta");
                        Location currentLocation = (Location) task.getResult();
                        goToLocationZoom(currentLocation.getLatitude(), currentLocation.getLongitude(), DEFAULT_ZOOM);
                    } else {
                        Log.v(TAG, "getLastLocation: izņēmums", task.getException());
                        Log.d(TAG, "onComplete: pašreizējā atrašanās vieta nav atrasta");
                        Toast.makeText(MapsActivity.this, "Atrašanās vieta nav atrasta!", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (SecurityException e) {
            Log.d(TAG, "getDeviceLocation: drošības izņēmums" + e.getMessage());
        }
    }

    private void setMarker(String locality, double lat, double lng) {

        MarkerOptions options = new MarkerOptions()
                .title(locality)
                .draggable(false)
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.marker_icon))
                .position(new LatLng(lat, lng));
        marker = mMap.addMarker(options);
    }

    private void punkti() {
        setMarker("Rīga, Eksporta iela 3", 56.95863, 24.09499);
        setMarker("Ogre, Brīvības iela 15", 56.81730, 24.60375);
        setMarker("Rīga, Bauskas iela 86", 56.91524, 24.11314);
        setMarker("Rīga, Brīvības gatve 372", 56.98177, 24.20540);
        setMarker("Gulbene, Brīvības iela 66", 57.17452, 26.74692);
        setMarker("Rīga, Kārļa Ulmaņa gatve 86", 56.92782, 24.03267);
        setMarker("Rīga, Ganību dambis 19a", 56.97743, 24.10978);
        setMarker("Talsu novads, Ģibuļu pagasts, „Dumbri” (Spāre)", 57.21861, 22.24824);
        setMarker("Jūrmala, 36.līnija", 56.99782, 23.90046);
        setMarker("Tērvete, Tērvetes dabas parks", 56.48916, 23.373034);
        setMarker("Rīga, S.Eizenšteina iela 16", 56.96617, 24.23277);
        setMarker("Sigulda, Stacijas laukums, Ausekļa iela 6", 57.15353, 24.85603);
        setMarker("Liepāja, Rožu iela 37", 56.50373, 20.99888);
    }

    private void goToLocationZoom(double lat, double lng, float zoom) {
        LatLng vieta = new LatLng(lat, lng);
        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(vieta, zoom);
        mMap.moveCamera(update);
        Log.d(TAG, "moveCamera: tiek pārvietots kameras skats uz lat: " +
                vieta.latitude + ", lng: " + vieta.longitude);
    }

    public void geoLocate(View view) throws IOException {

        EditText et = findViewById(R.id.editText);
        String location = et.getText().toString();

        Geocoder gc = new Geocoder(this);
        List<Address> list = gc.getFromLocationName(location, 1);
        Address address = list.get(0);
        String locality = address.getLocality();

        Toast.makeText(this, locality, Toast.LENGTH_LONG).show();

        double lat = address.getLatitude();
        double lng = address.getLongitude();
        goToLocationZoom(lat, lng, DEFAULT_ZOOM);

        setMarker(locality, lat, lng);

    }

    private String getDirectionsUrl(LatLng origin,LatLng dest){

        String str_origin = "origin="+origin.latitude+","+origin.longitude;
        String str_dest = "destination="+dest.latitude+","+dest.longitude;
        String parameters = str_origin+"&"+str_dest+"&";
        String key = getResources().getString(R.string.google_maps_key);
        String url = "https://maps.googleapis.com/maps/api/directions/json?"+parameters+"key="+key+"&callback";
        return url;
    }

    private String downloadUrl(String strUrl) throws IOException{
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try{
            URL url = new URL(strUrl);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.connect();
            iStream = urlConnection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
            StringBuffer sb  = new StringBuffer();
            String line = "";
            while( ( line = br.readLine())  != null){
                sb.append(line);
            }

            data = sb.toString();
            br.close();

        }catch(Exception e){
            Log.d("Error downloading url", e.toString());
        }finally{
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()){
            case R.id.mapTypeNone:
                mMap.setMapType(GoogleMap.MAP_TYPE_NONE);
                break;
            case R.id.mapTypeNormal:
                mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                break;
            case R.id.mapTypeSatellite:
                mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                break;
            case R.id.mapTypeTerrain:
                mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                break;
            case R.id.mapTypeHybrid:
                mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                break;

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressLint("StaticFieldLeak")
    private class DownloadTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {

            String data = "";
            try{
                data = downloadUrl(url[0]);
            }catch(Exception e){
                Log.d("Background Task",e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            ParserTask parserTask = new ParserTask();
            parserTask.execute(result);
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String,String>>>> {

        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try{
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();
                routes = parser.parse(jObject);
            }catch(Exception e){
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();

            // Traversing through all the routes
            for(int i=0;i<result.size();i++){
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for(int j=0;j<path.size();j++){
                    HashMap<String,String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(15);
                lineOptions.color(Color.RED);
            }

            // Drawing polyline in the Google Map for the i-th route
            mMap.addPolyline(lineOptions);
        }
    }

}
